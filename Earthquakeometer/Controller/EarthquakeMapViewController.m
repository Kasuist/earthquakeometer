//
//  EarthquakeMapViewController.m
//  Earthquakeometer
//
//  Created by Beau on 18/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "EarthquakeMapViewController.h"
#import <MapKit/MapKit.h>
#import "EarthquakeFetcher.h"
#import "Quake.h"

@interface EarthquakeMapViewController()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation EarthquakeMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadLatestEarthquakes];
}

- (void)loadLatestEarthquakes {
    EarthquakeFetcher *fetcher = [[EarthquakeFetcher alloc] init];
    [fetcher fetchEarthquakes:^(NSArray *earthquakes) {

        // Lets have the map zoom into a random location
        Quake *firstEarthquake = earthquakes[arc4random() % earthquakes.count];
        
        CLLocationCoordinate2D zoomLocation;
        zoomLocation.latitude = firstEarthquake.lat;
        zoomLocation.longitude= firstEarthquake.lon;
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 2000000, 2000000);
        
        [_mapView setRegion:viewRegion animated:YES];
        [self plotEarthquakes:earthquakes];
    }];
}

- (void)plotEarthquakes:(NSArray *)earthquakes {
    for (Quake *quake in earthquakes) {
        [_mapView addAnnotation:quake];
    }
}


@end
