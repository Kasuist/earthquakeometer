//
//  EarthquakeFetcher.m
//  Earthquakeometer
//
//  Created by Beau on 18/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "EarthquakeFetcher.h"
#import "Quake.h"

@implementation EarthquakeFetcher

- (void)fetchEarthquakes:(void(^)(NSArray *earthquakes))earthquakes {
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:@"http://www.seismi.org/api/eqs/"];
    [[session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error) {
            NSError *error;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSMutableArray *parsedEarthquakes = [NSMutableArray array];
            for (NSDictionary *item in responseDictionary[@"earthquakes"]) {
                Quake *quake = [[Quake alloc] init];
                for (NSString *key in item.allKeys) {
                    [quake setValue:item[key] forKey:key];
                }
                [parsedEarthquakes addObject:quake];
            }
            earthquakes(parsedEarthquakes);
        }
        else {
            NSLog(@"%s, %@", __PRETTY_FUNCTION__, error.debugDescription);
        }
    }] resume];
}

@end
