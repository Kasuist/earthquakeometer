//
//  Quake.m
//  Earthquakeometer
//
//  Created by Beau on 18/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "Quake.h"

@implementation Quake

- (NSString *)title {
    return _region;
}

- (NSString *)subtitle {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:_timedate];
    [formatter setDateFormat:@"HH:mm:ss, EE, d/M/y"];
    NSString *stringDate = [formatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"Magnitude of %.2f at %@", _magnitude, stringDate];
}

- (CLLocationCoordinate2D)coordinate {
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(_lat, _lon);
    return coord;
}

@end
