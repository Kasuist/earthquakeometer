//
//  EarthquakeFetcher.h
//  Earthquakeometer
//
//  Created by Beau on 18/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EarthquakeFetcher : NSObject

- (void)fetchEarthquakes:(void(^)(NSArray *earthquakes))earthquakes;

@end
