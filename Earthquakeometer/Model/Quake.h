//
//  Quake.h
//  Earthquakeometer
//
//  Created by Beau on 18/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface Quake : NSObject <MKAnnotation>

@property (strong, nonatomic) NSString *src;
@property (strong, nonatomic) NSString *eqid;
@property (strong, nonatomic) NSString *timedate;
@property (nonatomic) CGFloat lat;
@property (nonatomic) CGFloat lon;
@property (nonatomic) CGFloat magnitude;
@property (nonatomic) CGFloat depth;
@property (strong, nonatomic) NSString *region;

@end
