#Earthquake-O-meter#

I tried to tackle this a little differently to how I think most people would have. Rather than go all out, and add plenty of options, I decided to keep it as simple as possible, while still allowing for the code to be highly reusable. 

The app does one thing, and one thing well.
Shows the earthquake occurrences on a map as documented by http://www.seismi.org.

Thats it.
